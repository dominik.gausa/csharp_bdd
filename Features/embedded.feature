Feature: Embedded

Scenario Outline: Signal Loop
  Given Device Simple
  When sig_loop is set to <val>
  Then sig_loop is <val_res>

  Examples: 
    | val | val_res |
    | 1   | 1       |
    | 2   | 2       |
    | -1  | -1      |



Scenario Outline: Heating Basic
  Given Device Simple
  When set_temp_min is set to <setmin>
  And set_temp_max is set to <setmax>
  And sig_temperature is set to <temp>
  And sig_enable is set to 1
  Then wait for 1s
  And sig_heateractive is <heating>

  Examples:
    | setmin | setmax | temp | heating |
    |  250   |    750 |  100 |       1 |
    |  250   |    750 |  800 |       0 |

Scenario: Heating off when not enabled
  Given Device Simple
  When set_temp_min is set to <setmin>
  And set_temp_max is set to <setmax>
  And sig_temperature is set to <temp>
  And sig_enable is set to 0
  Then wait for 1s
  And sig_heateractive is <heating>

  Examples:
    | setmin | setmax | temp | heating |
    |  250   |    750 |  100 |       0 |
    |  250   |    750 |  800 |       0 |


Scenario: Hysteresis
  Given Device Simple
  When set_temp_min is set to 250
  And set_temp_max is set to 750
  And sig_enable is set to 1
  And Device is regulating
  And running for 20s
  Then stat_temperature_min is below 350
  And stat_temperature_max is above 650


Scenario Outline: Settings Write and Read
  Given Device Simple
  When set_temp_min is set to <setmin>
  And set_temp_max is set to <setmax>
  Then set_temp_min is <setmin>
  And set_temp_max is <setmax>

  Examples:
    | setmin | setmax |
    |  250   |    750 |
    |  350   |    550 |
