using NUnit.Framework;
using TechTalk.SpecFlow;
using System.Collections.Generic;
using System.IO.Ports;
using System;
using System.Text.RegularExpressions;

namespace SpecFlowDemo.Steps
{

  [Binding]
  public class EmbeddedStepDefinitions
  {
    private readonly ScenarioContext _scenarioContext;
    interface ISignal
    {

      void Set(float value);
      float Get();
    };

    class SignalStore : ISignal
    {
      float m_store = 0;

      public SignalStore() { }

      public virtual void Set(float value) => (m_store) = (value);
      public virtual float Get()
      {
        return m_store;
      }
    };

    class Terminal {
      SerialPort _serialPort;
      static Terminal instance;

      public static ref Terminal Instance() {
        if(instance == null)
          instance = new Terminal();
        return ref instance;
      }


      Terminal(){
        Console.Error.WriteLine("Available Ports:");
        foreach (string s in SerialPort.GetPortNames())
        {
          Console.Error.WriteLine("   {0}", s);
        }

        _serialPort = new SerialPort();
        _serialPort.PortName = "COM3";
        _serialPort.BaudRate = 115200;
        _serialPort.DataBits  = 8;
        _serialPort.Parity = Parity.None;
        _serialPort.StopBits = StopBits.One;
        _serialPort.ReadTimeout = 5000;
        _serialPort.WriteTimeout = 1000;
        _serialPort.Open();
      }

      public bool Write(string line) {
        try{
          _serialPort.WriteLine(line);
          return true;
        }catch(Exception ex) {
          System.Console.Error.WriteLine(ex);
          return false;
        }
      }

      public string ReadLine() {
        string line = "";
        try{
          line =_serialPort.ReadLine();
          line = line.Replace("\r", "");
        }catch(Exception ex) {
          System.Console.Error.WriteLine(ex);
        }
        return line;
      }

      public void Flush() {
        _serialPort.DiscardInBuffer();
      }
    };

    Dictionary<string, ISignal> signals = new Dictionary<string, ISignal>();
    Dictionary<string, float> stats = new Dictionary<string, float>();

    class ElecSignal : ISignal
    {
      string name;

      public ElecSignal(string name) {
        this.name = name;
      }

      public virtual void Set(float value) {
        Terminal.Instance().Write(string.Format("io set {0} {1:d}", name, (int)value));
      }

      public virtual float Get()
      {
        Terminal.Instance().Flush();
        Terminal.Instance().Write(string.Format("io get {0}", name));
        Terminal.Instance().ReadLine();
        string line = Terminal.Instance().ReadLine();
        line = line.Replace("\r", "");
        float value = 0;
        try{
          value = float.Parse(line);
        }catch(Exception ex){
          Console.Error.WriteLine(string.Format("io get {0}", name));
          Console.Error.WriteLine(line);
          Console.Error.WriteLine(ex);
        }
        return value;
      }
    };

    public class Setting : ISignal
    {
      public enum Settings {
        CALLBACK,
        TEMPERATURE_MIN,
        TEMPERATURE_MAX,
        CONTROL_LOG_ENABLE,
        MODCOM_LOG_ENABLE,
        SIZE
      };

      int func;
      int subFunc;

      public Setting(int func, int subFunc) {
        this.func = func;
        this.subFunc = subFunc;
      }

      public virtual void Set(float value) {
        Terminal.Instance().Write(string.Format("io modcom {0} {1:d} {2:d}", this.func, this.subFunc, (int)value));
      }

      public virtual float Get()
      {
        int value = 0;

        EmbeddedStepDefinitions.SetTesterConfig(Settings.CONTROL_LOG_ENABLE, 0);
        EmbeddedStepDefinitions.SetTesterConfig(Settings.MODCOM_LOG_ENABLE, 1);
        Terminal.Instance().Write(string.Format("io modcom {0} {1} {2}", this.func, 0, this.subFunc));

        //[dataParser] Fun: 1, SubFun: 0, Val: 345
        Regex rx = new Regex(@"\[dataParser\] Fun: (\d+), SubFun: (\d+), Val: (\d+)",
          RegexOptions.Compiled);
      
        var start = DateTime.Now;
        while(DateTime.Now.Subtract(start).TotalSeconds < 2) {
          string line = Terminal.Instance().ReadLine();
          //Console.Error.WriteLine(line);

          Match match = rx.Match(line);
          if(match.Success) {
            int func = -1, subFunc = -1;
            int.TryParse(match.Groups[1].Value, out func);
            int.TryParse(match.Groups[2].Value, out subFunc);
            int.TryParse(match.Groups[3].Value, out value);

            if(this.func == func && this.subFunc == subFunc) {
              int.TryParse(match.Groups[3].Value, out value);
              break;
            }
          }
        }

        EmbeddedStepDefinitions.SetTesterConfig(Settings.MODCOM_LOG_ENABLE, 0);

        return value;
      }
    };



    public EmbeddedStepDefinitions(ScenarioContext scenarioContext)
    {
      _scenarioContext = scenarioContext;
      signals.Add("sig_loop", new SignalStore());

      signals.Add("sig_led0", new ElecSignal("led0"));
      signals.Add("sig_led1", new ElecSignal("led1"));
      signals.Add("sig_enable", new ElecSignal("led2"));
      signals.Add("sig_led3", new ElecSignal("led3"));
      signals.Add("sig_heateractive", new ElecSignal("in0"));
      signals.Add("sig_in1", new ElecSignal("in1"));
      
      signals.Add("sig_aIn0", new ElecSignal("aIn0"));
      signals.Add("sig_aIn1", new ElecSignal("aIn1"));
      signals.Add("sig_temperature", new ElecSignal("aOut0"));

      
      signals.Add("set_temp_min", new Setting(0, (int)Setting.Settings.TEMPERATURE_MIN));
      signals.Add("set_temp_max", new Setting(0, (int)Setting.Settings.TEMPERATURE_MAX));
      signals.Add("set_control_log_enable", new Setting(0, (int)Setting.Settings.CONTROL_LOG_ENABLE));
      signals.Add("set_modcom_log_enable", new Setting(0, (int)Setting.Settings.MODCOM_LOG_ENABLE));
    }


    public static void SetTesterConfig(Setting.Settings setting, int value) {
      Terminal.Instance().Flush();
      Terminal.Instance().Write($"io config {(int)setting} {value}");
      Terminal.Instance().ReadLine();
      Terminal.Instance().ReadLine();
    }

    public static void SetTesterDeviceState(string state) {
      Terminal.Instance().Flush();
      Terminal.Instance().Write($"io devicestate {state}");
      Terminal.Instance().ReadLine();
      Terminal.Instance().ReadLine();
    }

    [Given(@"Device Simple")]
    public void GivenDeviceSimple()
    {
      SetTesterConfig(Setting.Settings.CONTROL_LOG_ENABLE, 0);
      SetTesterConfig(Setting.Settings.MODCOM_LOG_ENABLE, 0);
      SetTesterDeviceState("TESTFRAMEWORK");
    }


    [When(@"Device is regulating")]
    public void GivenDeviceRegulating()
    {
      SetTesterConfig(Setting.Settings.CONTROL_LOG_ENABLE, 0);
      SetTesterConfig(Setting.Settings.MODCOM_LOG_ENABLE, 0);
      SetTesterDeviceState("HW_MOCK");
    }

    [When(@"(sig_.*) is set to (.*)")]
    public void WhenSignalIsSetTo(string signal, float value)
    {
      if (signals.ContainsKey(signal))
      {
        signals[signal].Set(value);
      }
      else Assert.Fail("Unknown signal");
    }

    [When(@"(set_.*) is set to (.*)")]
    public void WhenSettingIsSetTo(string signal, float value)
    {
      if (signals.ContainsKey(signal))
      {
        signals[signal].Set(value);
      }
      else Assert.Fail("Unknown signal");
    }

    [When(@"running for (.*)s")]
    public void WhenRunning(float value)
    {
      //19:53.005,000] <inf> main: [hw_mock] IsHeating: 1 Temperature: 350
      Regex rx = new Regex(@"\[hw_mock\] IsHeating: (\d+) Temperature: (\d+)",
          RegexOptions.Compiled | RegexOptions.IgnoreCase);

      SetTesterConfig(Setting.Settings.CONTROL_LOG_ENABLE, 1);
      SetTesterDeviceState("HW_MOCK");

      var start = DateTime.Now;
      while(DateTime.Now.Subtract(start).TotalSeconds < value) {
        string line = Terminal.Instance().ReadLine();
        //Console.Error.WriteLine(line);
        MatchCollection matches = rx.Matches(line);
        foreach (Match match in matches)
        {
          var isHeating = float.Parse(match.Groups[1].Value);
          var temperature = float.Parse(match.Groups[2].Value);
          if(!stats.ContainsKey("stat_temperature_max") 
              || stats["stat_temperature_max"] < temperature)
            stats["stat_temperature_max"] = temperature;

          if(!stats.ContainsKey("stat_temperature_min")
              || stats["stat_temperature_min"] > temperature)
            stats["stat_temperature_min"] = temperature;
        }
      }

      SetTesterConfig(Setting.Settings.CONTROL_LOG_ENABLE, 0);
      SetTesterDeviceState("TESTFRAMEWORK");
    }

    [Then(@"wait for (.*)s")]
    public void ThenWait(float value)
    {
      System.Threading.Thread.Sleep((int)(value * 1000));
    }


    [Then(@"(sig_.*) is (.*)")]
    public void ThenSignalXIsY(string signal, float value)
    {
      if (signals.ContainsKey(signal))
      {
        Assert.AreEqual(value, signals[signal].Get());
      }
      else Assert.Fail("Unknown signal");
    }

    [Then(@"(set_.*) is (.*)")]
    public void ThenSettingXIsY(string setting, float value)
    {
      if (signals.ContainsKey(setting))
      {
        Assert.AreEqual(value, signals[setting].Get());
      }
      else Assert.Fail("Unknown setting");
    }

    
    [Then(@"(stat_.*) is (.*)")]
    public void ThenStatXIsY(string stat, float value)
    {
      if (stats.ContainsKey(stat))
      {
        Assert.AreEqual(stats[stat], value);
      }
      else Assert.Fail("Unknown stat");
    }

    [Then(@"(stat_.*) is below (.*)")]
    public void ThenStatXIsBelowY(string stat, float value)
    {
      if (stats.ContainsKey(stat))
      {
        Assert.LessOrEqual(stats[stat], value);
      }
      else Assert.Fail("Unknown stat");
    }


    [Then(@"(stat_.*) is above (.*)")]
    public void ThenStatXIsAboveY(string stat, float value)
    {
      if (stats.ContainsKey(stat))
      {
        Assert.GreaterOrEqual(stats[stat], value);
      }
      else Assert.Fail("Unknown stat");
    }

    [AfterScenario]
    public void LogStats()
    {
      foreach(var stat in stats) {
        Console.Error.WriteLine("  {0}: {1}", stat.Key, stat.Value);
      }
    }
  }
}